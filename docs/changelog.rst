.. _changelog:


#########
Changelog
#########

All releases are available in the project `releases page`_.

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_, and this project will adheres to
`Semantic Versioning`_ from version 1.0 and after.

.. _releases page: https://gitlab.com/exfo/products/tandm/basecamp/sphinxexfo/-/releases
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html


..
    How do I make a good changelog?
    ===============================

    Guiding Principles
    ------------------

    - Changelogs are for humans, not machines.
    - There should be an entry for every single version.
    - The same types of changes should be grouped.
    - Versions and sections should be linkable.
    - The latest version comes first.
    - The release date of each version is displayed.
    - Mention whether you follow Semantic Versioning.

    Types of changes
    ----------------

    - **Added** for new features.
    - **Changed** for changes in existing functionality.
    - **Deprecated** for soon-to-be removed features.
    - **Removed** for now removed features.
    - **Fixed** for any bug fixes.
    - **Security** in case of vulnerabilities.

    [1.0.0] - 2017-06-20
    --------------------

    Added
    ~~~~~

    - Added a feature.


.. _release-next:

0.3 - 2024-08-13
==================

Added
-----

*   Table of content entries for constituents :issue:`4`.

Fixed
-----

*   Custom Index name displayed as `True` :issue:`3`.
*   Fixed backward incompatible changes from Gitlab CI


.. _release-0.2.2:

0.2.2 - 2023-12-19
==================

.. admonition:: Downloads

    :release:`0.2.2`

Fixed
-----

*   (internal) Duplicated name in namespace
*   No warning with duplicated namespaces of different objtypes


.. _release-0.2.1:

0.2.1 - 2023-12-16
==================

.. admonition:: Downloads

    :release:`0.2.1`

Added
-----

*   Configurable namespace separator


.. _release-0.1:

0.1 - 2022-04-04
================

.. admonition:: Downloads

    :release:`0.1`

Added
-----

*   Generation of directives and roles in a given domain.

*   A custom domain index that keep track of cross-references.

*   Nested constituent and relative cross-references.
